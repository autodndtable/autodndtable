# autodndtable

## About

Assign players to TTRPG tables based on tiers and required/ forbidden people combinations

## Prerequisites

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
      NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
      "OPTIONAL" in this document are to be interpreted as described in
      [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119).

## Getting Started

### Installation

#### As a node package

1. Add the package to your package.json file:

    ```bash
    npm install autodndtable
    ```

2. Include the package in your javascript

    ```javascript
    const autodndtable = require("autodndtable");
    ```

#### As a \<script>

1. Add \<script>-tag to your HTML:

    ```html
    <script src="https://cdn.jsdelivr.net/npm/autodndtable@latest/dist/bundle.js"></script>
    ```

## Usage

### Create the "accounts" json

```json
[
	{
		"uuid": "Alice",
		"tiers": ["1"],
		"match_with": ["Charlie"],
		"dont_match_with": []
	},
	{
		"uuid": "Bob",
		"tiers": ["2"],
		"match_with": [],
		"dont_match_with": []
	},
	{
		"uuid": "Charlie",
		"tiers": ["1", "2"],
		"match_with": [],
		"dont_match_with": []
	}
]
```

| key | type | value |
| :---: | :---: | :---: |
| uuid | string | A unique identifier for the person. Can be a username, email or unique id |
| tiers | array(string\|integer) | The tiers in which this person wishes to play |
| match_with | array(string) | The uuid of other people to which this player MUST be paired by the script |
| dont_match_with | array(string) | The uuid of other people to which this player MUST NOT be paired by the script |

### Create the "tables" json

```json
[
	{
		"dm_uuid": "Hannah",
		"tiers": ["1"],
		"players_uuid": [],
		"match_with": [],
		"dont_match_with": ["Alice"],
		"max_players": 3
	},
	{
		"dm_uuid": "Isaac",
		"tiers": ["2"],
		"players_uuid": [],
		"match_with": [],
		"dont_match_with": [],
		"max_players": 3
	},
	{
		"dm_uuid": "Julia",
		"tiers": ["1", "2"],
		"players_uuid": [],
		"match_with": [],
		"dont_match_with": [],
		"max_players": 3
	}
]
```

| key | type | value |
| :---: | :---: | :---: |
| dm_uuid | string | A unique identifier for the person. Can be a username, email or unique id |
| tiers | array(string\|integer) | The tiers in which this DM can run a table |
| players_uuid | array(string\|integer) | The uuid of players assign to this table. **This array MUST be declared but empty.** It will be filled by the script. |
| match_with | array(string) | The uuid of other people to which this DM MUST be paired by the script |
| dont_match_with | array(string) | The uuid of other people to which this DM MUST NOT be paired by the script |
| max_players | integer | The maximum number of players that can be assigned to this table by the script |

### Run the script

```javascript
//include the script from node_modules
const autodndtable = require("autodndtable");

//See Usage#Create the "accounts" json for structure
let accounts = [{uuid:"Alice",tiers:["1"],match_with:["Charlie"],dont_match_with:[]},{uuid:"Bob",tiers:["2"],match_with:[],dont_match_with:[]},{uuid:"Charlie",tiers:["1","2"],match_with:[],dont_match_with:[]}];

//See Usage#Create the "tables" json for structure
let tables=[{dm_uuid:"Hannah",tiers:["1"],players_uuid:[],match_with:[],dont_match_with:["Alice"],max_players:3},{dm_uuid:"Isaac",tiers:["2"],players_uuid:[],match_with:[],dont_match_with:[],max_players:3},{dm_uuid:"Julia",tiers:["1","2"],players_uuid:[],match_with:[],dont_match_with:[],max_players:3}];

//Create the arrangement
//Will return null if no arrangement can be found
let arrangement = autodndtable.tryToFindArrangement(accounts, tables);

//Write the output to console
console.log(JSON.stringify(arrangement, false, "\t"))
```

## Examples

Examples for using this script as a package and in a plain HTML+JS website can be found on the [Gitlab page](https://gitlab.com/autodndtable/autodndtable/-/tree/master/examples).

## License

This project is licensed under the MIT License. See the [LICENSE](https://gitlab.com/autodndtable/autodndtable/-/blob/master/license.md) file for details.
