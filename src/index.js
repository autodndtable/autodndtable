const randomElement = (array) => array[Math.floor(Math.random() * array.length)];

function tryToFindArrangement(accounts, tables) {
	accounts = JSON.parse(JSON.stringify(accounts));
	tables = JSON.parse(JSON.stringify(tables));

	accounts = accounts.concat(tables.map(table => ({
		"uuid": table.dm_uuid,
		"tiers": table.tiers,
		"match_with": table.match_with ? table.match_with : [],
		"dont_match_with": table.dont_match_with ? table.dont_match_with : [],
	})));

	let arrangement = null;
	let i = 0;
	do {
		arrangement = generateArrangement(accounts, tables);
		i++;
	} while (!arrangement && i < 100);

	return arrangement;
}

function generateArrangement(accounts, tables, tryHighestPlayerTier = false, tryHighestTableTier = false) {
	if (!tryHighestPlayerTier && !tryHighestTableTier) {
		// tables = generateArrangement(accounts, tables, true, true);
		tables = generateArrangement(accounts, tables, false, true);
		// tables = generateArrangement(accounts, tables, true, false);
	}

	let accountUuidToSkip = [];
	while ((accountUuidToSkip.length < accounts.length) && (accounts.find(account => !tables.find(table => table.dm_uuid == account.uuid || table.players_uuid.includes(account.uuid))))) {
		//Order accounts by number of possible tables remaining
		let accountsByPossibleTables = {};

		accounts.filter(account => !accountUuidToSkip.includes(account.uuid)).forEach(account => {
			if (tables.find(table => table.dm_uuid == account.uuid || table.players_uuid.includes(account.uuid))) {
				accountUuidToSkip.push(account.uuid);
				return;
			}
			
			account.possible_tables = getPossibleTables(accounts, account, tables, tryHighestPlayerTier, tryHighestTableTier);
			if (accountsByPossibleTables[account.possible_tables.length] == undefined) accountsByPossibleTables[account.possible_tables.length] = [];
			accountsByPossibleTables[account.possible_tables.length].push(account.uuid);
		});

		let accountsByPossibleTablesKeys = Object.keys(accountsByPossibleTables);
		accountsByPossibleTablesKeys.sort();
		const accountsByPossibleTablesFirstKey = accountsByPossibleTablesKeys[0];
		if (accountsByPossibleTablesKeys[0] == undefined) return tables;
		if (accountsByPossibleTablesFirstKey == 0) {
			//Account with no possible tables
			//Arrangement has failed
			if (!tryHighestPlayerTier && !tryHighestTableTier) {
				// console.log(tables);
				// console.log(accountsByPossibleTables[0]);
				return null;
			} else {
				accountUuidToSkip = accountUuidToSkip.concat(accountsByPossibleTables[0]);
			}
		} else {
			const accountUuidToAssignTable = randomElement(accountsByPossibleTables[accountsByPossibleTablesFirstKey]);
			const accountToAssignTable = accounts.find(account => account.uuid == accountUuidToAssignTable);

			let possibleTablesByNrOfPlayers = {};
			accountToAssignTable.possible_tables.forEach(possibleTableDmUuid => {
				const possibleTable = tables.find(table => table.dm_uuid == possibleTableDmUuid);
				
				if (possibleTablesByNrOfPlayers[possibleTable.players_uuid.length] == undefined) possibleTablesByNrOfPlayers[possibleTable.players_uuid.length] = [];
				possibleTablesByNrOfPlayers[possibleTable.players_uuid.length].push(possibleTableDmUuid);
			});

			let possibleTablesByNrOfPlayersKeys = Object.keys(possibleTablesByNrOfPlayers);
			possibleTablesByNrOfPlayersKeys.sort();
			const possibleTablesByNrOfPlayersFirstKey = possibleTablesByNrOfPlayersKeys[0];
			
			const tableDmUuidToAssignTo = randomElement(possibleTablesByNrOfPlayers[possibleTablesByNrOfPlayersFirstKey]);
			const tableToAssignTo = tables.find(table => table.dm_uuid == tableDmUuidToAssignTo);
			
			tableToAssignTo.players_uuid.push(accountUuidToAssignTable);
			tableToAssignTo.tiers = tableToAssignTo.tiers.filter(tier => accountToAssignTable.tiers.includes(tier));
			accountUuidToSkip.push(accountUuidToAssignTable);
		}
	}

	return tables;
}

function getPossibleTables(accounts, account, tables, tryHighestPlayerTier = false, tryHighestTableTier = false) {
	let ret = [];

	tables.forEach(table => {
		if ((tryHighestPlayerTier ? [Math.max(...account.tiers)] : account.tiers).find(tier => (tryHighestTableTier ? [Math.max(...table.tiers)] : table.tiers).includes(tier))) {
			//TableTiers and AccountTiers are compatible
			if (table.players_uuid.length < table.max_players) {
				if (!account.dont_match_with.find(dontMatchWithUuid => table.dm_uuid == dontMatchWithUuid || table.players_uuid.includes(dontMatchWithUuid))) {
					if (table.players_uuid.concat([table.dm_uuid]).every(accountUuid => {
						const accountUuidAccount = accounts.find(findAccount => findAccount.uuid == accountUuid);
						if (!accountUuidAccount) return true;
						return (!accountUuidAccount.dont_match_with.find(dontMatchWithUuid => dontMatchWithUuid == account.uuid));
					})) {
						if (account.match_with.every(matchWithUuid => {
							return table.players_uuid.concat([table.dm_uuid]).includes(matchWithUuid) || !tables.some(someTable => someTable.players_uuid.concat([someTable.dm_uuid]).includes(matchWithUuid));
						})) {
							if (accounts.every(someAccount => {
								return !someAccount.match_with.includes(account.uuid) ||
												!tables.find(findTable => findTable.dm_uuid == someAccount.uuid || findTable.players_uuid.includes(someAccount.uuid)) ||
												tables.find(findTable => (findTable.dm_uuid == someAccount.uuid || findTable.players_uuid.includes(someAccount.uuid)) && findTable.dm_uuid == table.dm_uuid);
							})) {
								ret.push(table.dm_uuid);
							}
						}
					}
				}
			}
		}
	})

	return ret;
}

module.exports = { tryToFindArrangement };