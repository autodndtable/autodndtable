const autodndtable = require("autodndtable");

let accounts = [
	{
		"uuid": "Alice",
		"tiers": ["1"],
		"match_with": [],
		"dont_match_with": []
	},
	{
		"uuid": "Bob",
		"tiers": ["2"],
		"match_with": [],
		"dont_match_with": []
	},
	{
		"uuid": "Charlie",
		"tiers": ["1", "2"],
		"match_with": [],
		"dont_match_with": []
	},
	{
		"uuid": "David",
		"tiers": ["1"],
		"match_with": [],
		"dont_match_with": ["Alice"]
	},
	{
		"uuid": "Edward",
		"tiers": ["2"],
		"match_with": [],
		"dont_match_with": []
	},
	{
		"uuid": "Fiona",
		"tiers": ["1", "2"],
		"match_with": [],
		"dont_match_with": []
	},
	{
		"uuid": "George",
		"tiers": ["1"],
		"match_with": ["Charlie"],
		"dont_match_with": []
	}
]
;

const tables = [
	{
		"dm_uuid": "Hannah",
		"tiers": ["1"],
		"players_uuid": [],
		"match_with": [],
		"dont_match_with": ["George"],
		"max_players": 3
	},
	{
		"dm_uuid": "Isaac",
		"tiers": ["2"],
		"players_uuid": [],
		"match_with": [],
		"dont_match_with": [],
		"max_players": 3
	},
	{
		"dm_uuid": "Julia",
		"tiers": ["1", "2"],
		"players_uuid": [],
		"match_with": [],
		"dont_match_with": [],
		"max_players": 3
	}
];

let arrangement = autodndtable.tryToFindArrangement(accounts, tables);
console.log(JSON.stringify(arrangement, false, "\t"))